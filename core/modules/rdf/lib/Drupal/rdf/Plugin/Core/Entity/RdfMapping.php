<?php

/**
 * @file
 * Contains \Drupal\rdf\Plugin\Core\Entity\RdfMapping.
 */

namespace Drupal\rdf\Plugin\Core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\EntityType;
use Drupal\Core\Annotation\Translation;

/**
 * Config entity for working with RDF mappings.
 *
 * @EntityType(
 *   id = "rdf_mapping",
 *   label = @Translation("RDF mapping"),
 *   module = "rdf",
 *   controllers = {
 *     "storage" = "Drupal\Core\Config\Entity\ConfigStorageController"
 *   },
 *   config_prefix = "rdf.mapping",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class RdfMapping extends ConfigEntityBase {

  /**
   * Unique ID for the config entity.
   *
   * @var string
   */
  public $id;

  /**
   * Unique UUID for the config entity.
   *
   * @var string
   */
  public $uuid;

  /**
   * Entity type to be mapped.
   *
   * @var string
   */
  public $targetEntityType;

  /**
   * Bundle to be mapped.
   *
   * @var string
   */
  public $bundle;

  /**
   * The RDF type mapping for this bundle.
   *
   * @var array
   */
  protected $types;

  /**
   * The mappings for fields on this bundle.
   *
   * @var array
   */
  protected $fieldMappings;

  /**
   * Gets the mapping for the bundle-level data.
   *
   * @return array|null
   *   The field mapping, or NULL if there is no mapping.
   */
  public function getBundleMapping() {
    $types = array();
    if (isset($this->types)) {
      $types = $this->types;
    }
    return array('types' => $types);
  }

  /**
   * Sets the mapping for a bundle.
   *
   * This only sets bundle-level mappings, such as the RDF type. Mappings for
   * a bundle's fields should be handled with setFieldMapping.
   *
   * Example usage:
   * -Map the 'article' bundle to 'sioc:Post'.
   * @code
   * rdf_get_mapping('node', 'article')
   *   ->setBundleMapping(array(
   *     'types' => array('sioc:Post'),
   *   ))
   *   ->save();
   * @endcode
   *
   * @param array $mapping
   *   The bundle mapping.
   *
   * @return \Drupal\rdf\Plugin\Core\Entity\RdfMapping
   *   The RdfMapping object.
   */
  public function setBundleMapping(array $mapping) {
    if (isset($mapping['types'])) {
      $this->types = $mapping['types'];
    }

    return $this;
  }

  /**
   * Gets the mapping for a field on the bundle.
   *
   * @param string $field_name
   *   The name of the field.
   *
   * @return array|null
   *   The field mapping, or NULL if there is no mapping.
   */
  public function getFieldMapping($field_name) {
    $field_mapping = array(
      'properties' => NULL,
      'datatype' => NULL,
      'datatype_callback' => NULL,
      'mapping_type' => NULL,
    );
    if (isset($this->fieldMappings[$field_name])) {
      $field_mapping = array_merge($field_mapping, $this->fieldMappings[$field_name]);
    }
    return $field_mapping;
  }

  /**
   * Sets the mapping for a field.
   *
   * @param string $field_name
   *   The name of the field.
   * @param array $mapping
   *   The field mapping.
   *
   * @return \Drupal\rdf\Plugin\Core\Entity\RdfMapping
   *   The RdfMapping object.
   */
  public function setFieldMapping($field_name, array $mapping = array()) {
    $this->fieldMappings[$field_name] = $mapping;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->targetEntityType . '.' . $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    // Build an ID if none is set.
    if (empty($this->id)) {
      $this->id = $this->id();
    }
    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function getExportProperties() {
    $names = array(
      'id',
      'uuid',
      'targetEntityType',
      'bundle',
      'types',
      'fieldMappings',
    );
    $properties = array();
    foreach ($names as $name) {
      $properties[$name] = $this->get($name);
    }
    return $properties;
  }

}
